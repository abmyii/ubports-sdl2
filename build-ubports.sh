ARCH=$1
MACHINE=$2
HOST=$(gcc -dumpmachine)
apt-get update
apt-get install pkg-config devscripts checkinstall libgl1-mesa-dev:$ARCH libglu-dev:$ARCH libasound2-dev:$ARCH libpulse-dev:$ARCH libudev-dev:$ARCH yasm:$ARCH libsdl2-2.0-0:$ARCH libsdl2-dev:$ARCH libudev-dev:$ARCH libudev1:$ARCH libglib2.0-0:$ARCH libglib2.0-dev:$ARCH libegl1-mesa:$ARCH libegl1-mesa-dev:$ARCH libgles2-mesa-dev:$ARCH libmirclient-dev:$ARCH libxkbcommon-dev:$ARCH libvpx-dev:$ARCH libvpx3:$ARCH libpng16-dev:$ARCH libpng16-16:$ARCH libvorbis-dev:$ARCH libvorbis0a:$ARCH libwayland-client0:$ARCH libwayland-cursor0:$ARCH libwayland-dev:$ARCH libwayland-server0:$ARCH libmirwayland0:$ARCH libmirwayland-dev:$ARCH libxkbcommon-dev:$ARCH libxkbcommon0:$ARCH wayland-protocols -y
apt-get install debhelper dh-autoreconf fcitx-libs-dev libibus-1.0-dev -y

apt-get build-dep libsdl2:$ARCH

export PKG_CONFIG_PATH="/usr/lib/${MACHINE}/pkgconfig":"/usr/lib/pkgconfig":"/usr/share/pkgconfig"

mkdir build
cd build

echo $HOST
echo $ARCH
echo $MACHINE
../configure --enable-video-wayland --enable-video-wayland-qt-touch --enable-wayland-shared --enable-video-mir --enable-mir-shared --host=$MACHINE --build=$HOST --prefix=$PWD/TEST
make -j4
make install

cd ..

debuild -a$ARCH -i -us -uc -b
debuild clean

ls -alh
